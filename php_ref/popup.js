var acomp = document.getElementById('autocomplete');
var bg = chrome.extension.getBackgroundPage();
var tb = document.getElementById('func');
tb.addEventListener('keyup', function(e) {
  removeFunctionName();
  if (tb.value.length == 0) {
    return;
  }
  for ( var i in bg.php_functions_hash) {
    re = new RegExp("^" + tb.value);
    if (i.match(re)) {
      a = document.createElement('a');
      a.innerHTML = i;
      a.href = '#';
      a.setAttribute('function_name', i);
      a.setAttribute('title', bg.php_functions_hash[i]);
      a.onclick = function() {
        postFunctionName(this);
      };
      acomp.appendChild(a);
      acomp.appendChild(document.createElement('br'));
    }
  }
});

var postFunctionName = function(a) {
  var str = a.getAttribute('function_name');
  tb.value = str;
  document.forms['form_func'].submit();
}

var removeFunctionName = function() {
  len = acomp.childNodes.length;
  for ( var i = len - 1; i >= 0; i--) {
    acomp.removeChild(acomp.childNodes.item(i));
  }
}